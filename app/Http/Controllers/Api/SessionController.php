<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class SessionController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api', ['except' => 'register']);
    }

    //
    public function register(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string',
            'lastname' => 'required|string',
            'username' => 'required|string|unique:users',
            'password' => 'required|string|confirmed',
            'phone_number' => 'required|string|unique:users',
            'birthday' => 'required|date',
            'terms_and_conditions' => 'required|boolean|accepted',
            'privacy_notice' => 'required|boolean|accepted'
        ]);

        if($validator->fails()){
            return response()->json($validator->getMessageBag(), 400);
        }

        $user = User::create($request->only([
            'name',
            'lastname',
            'username',
            'password',
            'phone_number',
            'birthday',
            'terms_and_conditions',
            'privacy_notice'
        ]));

        $request->request->add([
            'client_id' => env('GRANT_PASSWORD_CLIENT_ID'),
            'client_secret' => env('GRANT_PASSWORD_CLIENT_SECRET'),
            'scope' => '*',
            'grant_type' => 'password',
            'username' => $user->username,
            'password' => $request->get("password")
        ]);

        Log::info("Request to oauth:");
        Log::info($request->toArray());

        $loginRequest = Request::create('api/oauth/token', 'POST');

        return \Route::dispatch($loginRequest);
    }

    public function profile(Request $request){
        return response()->json($request->user());
    }

    public function logout(Request $request){
        $request->user()->token()->revoke();

        return response()->json(null, 204);
    }
}
