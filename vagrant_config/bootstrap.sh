#!/usr/bin/env bash
apt-get purge `dpkg -l | grep php| awk '{print $2}' |tr "\n" " "`
add-apt-repository ppa:ondrej/php
apt-get -y update
apt-get -y upgrade
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
apt-get install -y php7.2 php7.2-common php7.2-opcache php7.2-mysql php7.2-mbstring php7.2-xml php7.2-zip php7.2-gd curl nodejs npm apache2 mysql-server mysql-client
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
chown -R $USER ~/.composer/
composer global require "laravel/installer"
if ! [ -L /var/www ]; then
	rm -rf /var/www/html
	ln -fs /vagrant/public /var/www/html
	a2enmod rewrite
fi
#create db
dbname=$(grep -o "^DB_DATABASE=\(.*\)" /vagrant/.env | sed "s/DB_DATABASE=\(.*\)/\1/")
#echo $dbname
#echo "CREATE DATABASE IF NOT EXISTS \`$dbname\`\n;"
mysql -u root --password=root -e "CREATE DATABASE IF NOT EXISTS \`$dbname\`\n;"
cd /vagrant
#rm /vagrant/public/storage
composer install
# npm install
# npm install --global gulp-cli
sed -i 's/export APACHE_RUN_USER=www-data/export APACHE_RUN_USER=vagrant/' /etc/apache2/envvars
sed -i 's/export APACHE_RUN_GROUP=www-data/export APACHE_RUN_GROUP=vagrant/' /etc/apache2/envvars
sed -i '/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride all/' /etc/apache2/apache2.conf
service apache2 restart
# php artisan key:generate
# php artisan storage:link
php artisan migrate
php artisan db:seed
php artisan passport:keys
php artisan passport:client --password
